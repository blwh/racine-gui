# GUI interface
import tkinter as tk
from tkinter import ttk
# To handle paths unspecific to OS
import ntpath
# For more image handling
from PIL import Image, ImageTk
# Opening files from computer
from tkinter.filedialog import askopenfilename, askopenfilenames

# Application class
class App(tk.Tk):

    # Dimension for the different frames
    appDim = (900, 600)
    menuDim = (appDim[0], 40)
    bDim = (80, 50)
    # Vector with image paths
    images = ()
    # Index for listbox
    listIndex = 0

    def __init__(self):
        tk.Tk.__init__(self)
        self._frame = None
        self.switch_frame(MainPage)
        # Styles
        ttk.Style().configure('TButton')

        Menu(self).place(anchor = tk.NW,
                         x = 0, y = 0,
                         width = App.menuDim[0], 
                         height = App.menuDim[1])

    def switch_frame(self, frame_class):
        """Destroys current frame and replaces it with a new one."""
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.place(anchor = tk.NW,
                          x = 0, y = App.menuDim[1],
                          width = App.appDim[0],
                          height = App.appDim[1] - App.menuDim[1])

class Menu(tk.Frame):
    
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        # Button to open main page
        bMainPage = ttk.Button(self,
                               text = "Main",
                               style = "TButton",
                               command = lambda: master.switch_frame(MainPage))
        # Button to show info about the application
        bAbout = ttk.Button(self,
                            text = "About",
                            style = "TButton",
                            command = lambda: master.switch_frame(AboutPage))
        # Button to exit the program
        bExit = ttk.Button(self,
                           text = "Exit",
                           style = "TButton",
                           command = master.destroy)

        bMainPage.place(anchor = tk.NW,
                        x = 1, y = 1,
                        width = App.bDim[0],
                        height = App.menuDim[1])
        bExit.place(anchor = tk.NW,
                    x = App.menuDim[0] - 81, y = 1,
                    width = App.bDim[0],
                    height = App.menuDim[1])
        bAbout.place(anchor = tk.NW,
                     x = App.menuDim[0] - 162, y = 1,
                     width = App.bDim[0],
                     height = App.menuDim[1])

class MainPage(tk.Frame):

    def __init__(self, master):
        tk.Frame.__init__(self, master)

        # Initiating list for chosen images
        imageList = tk.Listbox(self, 
                               selectmode = tk.SINGLE)
        imageList.place(anchor = tk.NW,
                        x = 1, y = 1,
                        width = 180,
                        height = App.appDim[1] - App.menuDim[1] - 40)
        
        # Label for image
        imageLabel = tk.Label(self, image = "")
        imageLabel.place(anchor = tk.NW,
                         x = 181, y = 0)

        # Button to load images
        bLoadImages = ttk.Button(self,
                                text = "Load",
                                style = "TButton",
                                command = lambda: self.OpenFile(imageList))
        bClearImages = ttk.Button(self,
                                  text = "Clear",
                                  style = "TButton",
                  command = lambda: self.ClearList(imageList, imageLabel))

        bLoadImages.place(anchor = tk.NW,
                     x = 1, y = App.appDim[1] - App.menuDim[1] - 36,
                     height = 34, width = 90)
        bClearImages.place(anchor = tk.NW,
                     x = 92, y = App.appDim[1] - App.menuDim[1] - 36,
                     height = 34, width = 90)

        if (App.listIndex):
            self.FillList(imageList)

        imageList.bind("<<ListboxSelect>>", 
                       lambda e: self.ShowImage(imageLabel, imageList))

        print(App.listIndex)
        print(App.images)

    # Prompts user to choose images and adds to list
    def OpenFile(self, imageList):
        # Prompt to choose images
        filenames = askopenfilenames(title = "Choose images")
        # For all files
        for filename in filenames:
            if not filename in App.images:
                # Add to list
                App.images = App.images + (filename,)
                imageList.insert(App.listIndex, 
                                 ntpath.basename(filename))
                # Increment list index
                App.listIndex += 1
                #print(list_index) # Debugging, to be removed

    # Fills the list each time the frame is put to front
    def FillList(self, imageList):
        for i in range(0, App.listIndex):
            imageList.insert(i, 
                             ntpath.basename(App.images[i]))

    # Clears the list
    def ClearList(self, imageList, imageLabel):
        # Clears list and resets index
        imageList.delete(0, tk.END)
        # Reset global vars
        App.listIndex = 0
        App.images = ()
        imageLabel.config(image = "")

    def ShowImage(self, imageLabel, imageList):
        if (imageList.curselection()):
            fileName = App.images[imageList.curselection()[0]]
            image = Image.open(fileName)
            width, height = image.size
            if (height/width > 1):
                image = image.resize(
                        (618, int(618*height/width)), Image.ANTIALIAS) 
            else:
                image = image.resize(
                        (int(559*width/height), 559), Image.ANTIALIAS) 
            #The (250, 250) is (height, width)
            pic = ImageTk.PhotoImage(image)
            imageLabel.config(image = pic)
            imageLabel.image = pic



class AboutPage(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        info = "This is a work-in-progress for the GUI used in the RACINE project. Look forward to amazing functionality and features that will amaze scientists long in to the future."
    
        # Info about the program
        textInfo = tk.Label(self, 
                            text = info,
                            justify = tk.LEFT,
                            wraplength = App.appDim[0])
        textInfo.place(anchor = tk.NW)

if __name__ == "__main__":
    app = App()
    
    size = str(App.appDim[0]) + "x" + str(App.appDim[1])
    app.geometry(size)

    app.mainloop()

